﻿using System;
using System.Collections.Generic;

namespace HelloWord
{
    public class Dictionary
    {
        /* Create a new dictionary with satic values */
        public Dictionary<string, Element> BuildDictionary()
        {
            var elements = new Dictionary<string, Element>();

            AddToDictionary(elements, "K", "Potassium", 19);
            AddToDictionary(elements, "Ca", "Calcium", 20);
            AddToDictionary(elements, "Sc", "Scandium", 21);
            AddToDictionary(elements, "Ti", "Titanium", 22);

            return elements;
        }

        private static void AddToDictionary(Dictionary<string, Element> elements,
       string symbol, string name, int atomicNumber)
        {
            Element theElement = new Element();

            theElement.Symbol = symbol;
            theElement.Name = name;
            theElement.AtomicNumber = atomicNumber;

            elements.Add(key: theElement.Symbol, value: theElement);
        }

        public void FindInDictionary(Dictionary<string, Element> dic, string symbol)
        {
            if (dic.ContainsKey(symbol) == false)
            {
                Console.WriteLine(symbol + " not found");
            }
            else
            {
                Element theElement = dic[symbol];
                Console.WriteLine("found: " + theElement.Name);
            }
        }
    }
}
