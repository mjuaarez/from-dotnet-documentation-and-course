﻿using System;
using System.Collections.Generic;

namespace HelloWord
{
    public class Program
    {
        private const string Value = "\n";

        static void Main(string[] args)
        {
            var words = new List<string>();
            Console.WriteLine("What element do you want to search for?");
            var symbolSearch = Console.ReadLine();
            
            /*var MyDictionary = new Dictionary();
            Dictionary<string, Element> myDictionary = MyDictionary.BuildDictionary();

            foreach (KeyValuePair<string, Element> kvp in myDictionary)
            {
                Element theElem = kvp.Value;

                Console.WriteLine("key: " + kvp.Key);
                Console.WriteLine("values: " + theElem.Symbol + " " +
                    theElem.Name + " " + theElem.AtomicNumber);
            }*/

            var dictionary2 = new Dictionary2();

            dictionary2.addToDictionary("K", "Potassium", 19);
            dictionary2.addToDictionary("Ca", "Calcium", 20);
            dictionary2.addToDictionary("Sc", "Scandium", 21);
            dictionary2.addToDictionary("Ti", "Titanium", 22);
            Console.WriteLine("-------");
            Console.WriteLine("size dictionary: " + dictionary2.getSize());

            try
            {
                var elemAtKey = dictionary2.getElementAtKey(symbolSearch);
                Console.WriteLine("name: " + elemAtKey.Name);
                Console.WriteLine("symbol: " + elemAtKey.Symbol);
                Console.WriteLine("atomic value: " + elemAtKey.AtomicNumber);
            }
            catch (KeyNotFoundException ex)
            {
                Console.WriteLine(ex);
            }


            
            


            exitMenu();
        }

        private static void exitMenu()
        {
            Console.Write($"{Environment.NewLine}Press any key to exit...");
            Console.ReadKey(true);
        }
    }

    public class Element
    {
        public string Symbol { get; set; }
        public string Name { get; set; }
        public int AtomicNumber { get; set; }
    }
}
