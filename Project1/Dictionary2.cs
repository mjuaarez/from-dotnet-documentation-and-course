﻿using System;
using System.Collections.Generic;

namespace HelloWord
{
    public class Dictionary2
    {
        public Dictionary<string, Element> dictionary2;

        public Dictionary2() => dictionary2 = new Dictionary<string, Element>();

        public void addToDictionary(string symbol, string name, int atomicNumber)
        {
            Element thisElement = new Element();

            thisElement.Name = name;
            thisElement.Symbol = symbol;
            thisElement.AtomicNumber = atomicNumber;
            this.dictionary2.Add(key: symbol, value: thisElement);
        }

        public Element getElementAtKey(string key)
        {
            var element = dictionary2[key];

            return element;
        }

        public int getSize()
        {
            return this.dictionary2.Count;
        }

        public static implicit operator Dictionary<object, object>(Dictionary2 v)
        {
            throw new NotImplementedException();
        }
    }
}

public class Element
{
    public string Symbol { get; set; }
    public string Name { get; set; }
    public int AtomicNumber { get; set; }
}
